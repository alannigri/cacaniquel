package com.company;

public enum Slots {
    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    SETE(300);

    private int valor;

    Slots(int valor){
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }

}
