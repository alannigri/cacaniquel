package com.company;

import java.util.ArrayList;

public class Jogador {
    String nome = "Alan";

    public void jogar(){
        int qtdeSlots = 3; //quantidade de Slots que será sorteado
        Maquina maquina = new Maquina();
        ArrayList<String> sorteados = maquina.sortear(qtdeSlots);
        maquina.montarResultado(sorteados);
        maquina.somaPontuacao(sorteados);
    }

}
