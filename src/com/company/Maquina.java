package com.company;

import java.util.ArrayList;
import java.util.Random;

public class Maquina {
    IO saida = new IO();

    public ArrayList<String> sortear(int qtdeSlots) {
        ArrayList<String> sorteados = new ArrayList<>();
        Slots[] slots = Slots.values();
        Random random = new Random();
        for (int i = 0; i < qtdeSlots; i++) {
            int indiceSorteado = random.nextInt(slots.length);
            sorteados.add(slots[indiceSorteado].name());
        }
        return sorteados;
    }

    public void somaPontuacao(ArrayList<String> sorteados) {
        int soma = 0;
        for (String sorteado : sorteados) {
            soma += Slots.valueOf(sorteado).getValor();
        }
        saida.exibirSlots(soma);
    }

    public void montarResultado(ArrayList<String> sorteados) {
        ArrayList<String> temp = new ArrayList<>();
        for(String sorteado : sorteados){
            temp.add(sorteado + "=" + Slots.valueOf(sorteado).getValor());
        }
        saida.exibirResultado(temp);
    }
}
